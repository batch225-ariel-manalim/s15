// COMMENTS in JavaScript
//  - Two write comments, we use two forward slash for single line comments and two forward slash and two asterisks for multi-line comments.
// This is a single line comment.
/*
	This is a multi-line comment.
	To write them, We add two asterisks inside of the forward slashes and write comments in between them.
*/

// Variable
/*
	- Variables contain values that can be changed over the execution time of the program.
	- To declare a variable, we use the "let" keyword.
*/

/*let productName = 'desktop computer';
console.log(productName);*/

/*CONSTANTS*/
/*
	- Use constants for values that will not change.
*/

/*const deliveryFee = 30;
console.log(deliveryFee);*/

/*DATA TYPES*/

/*1. STRINGS*/
/*
	- Strings are a series of characters that create a word, a phrase, sentence, or anything related to "TEXT".
	- Strings in JavaScript can be written using a single quote ('') or a double quote ("")
	- On other programming languages, only the double qoute can be used for creating strings.
*/

let country = 'Philippines';
let province = "Metro Manila";

/*CONCATENATION*/
console.log(country + ", " + province);

/*2. NUMBERS*/
/*
	- Includes integers/whole numbers, decimal numbers, fraction, exponential notation
*/
let headCount = 26;
console.log(headCount)

let grade = 98.7;
console.log(grade)

let randomNumber = Math.floor(Math.random() * 10) + 1;
console.log(randomNumber)

/*3. BOOLEAN
	- Boolean values are logical values.
	- Boolean values can contain either "true" or  "false"
*/
let isMarried = false;
let isGoodConduct = true;
console.log(`isMarried: ${isMarried}`)
console.log(`isGoodConduct: ${isGoodConduct}`)

/*4. OBJECTS*/

	/*ARRAYS*/
	/*
		- They are a special kind of data that stores multiple values.
		- They are a special type of an object.
		- They can store different data types but is normally used to store similar data types.
	*/

	// Syntax:
		// let/const arrayName = [ElementA, ElementB, ElementC...]

	let grades = [ 98.7, 92.1, 90.2, 94.6];
	console.log(grades)
	console.log(grades[0])

	// Object Literals
	/*
		- Objects are another special kind of data type that mimics real world to objects/items
		- They are used to create complex data that contains pieces of information that are relevant to each other.
		- Every individual piece of information if called a property of the object

		Syntax:

		let/const objectName = {
			propertyA: value,
			propertyB: value
		}
	*/

	let personDetails = {
		fullname: 'Juan Dela Cruz',
		age: 35,
		isMarried: false,
		contact: ['+6398762154', '02151545'],
		address: {
			houseNumber: '345',
			city: 'Manila'
		}
	}
	console.log(personDetails)